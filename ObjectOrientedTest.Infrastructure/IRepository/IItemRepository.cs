﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Domain.Entity;
    
namespace ObjectOrientedTest.Infrastructure.IRepository
{
   public interface IItemRepository
    {
        Task Register(ItemDomain domain);
        Task<string> Retrieve(string _item_name);
        Task<int> GetType(string _item_name);
        Task Dregister(string _item_name);
    }
}
