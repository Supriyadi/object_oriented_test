﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectOrientedTest.Infrastructure.Options
{
   public class ApplicationConfiguration
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }

   public class ConnectionStrings
    {
        public string MySqlConnection { get; set; }
    }
}
