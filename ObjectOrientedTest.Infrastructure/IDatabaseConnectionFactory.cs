﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace ObjectOrientedTest.Infrastructure
{
   public interface IDatabaseConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}
