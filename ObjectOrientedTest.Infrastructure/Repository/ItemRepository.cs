﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Domain.Entity;
using ObjectOrientedTest.Infrastructure.IRepository;
using Dapper;

namespace ObjectOrientedTest.Infrastructure.Repository
{
    public class ItemRepository : IItemRepository
    {
        private readonly IDatabaseConnectionFactory _db;
        public ItemRepository(IDatabaseConnectionFactory db)
        {
            this._db = db;
        }
        public async Task Dregister(string _item_name)
        {
            using (var _connection = this._db.CreateConnection())
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("delete from tbl_items where ItemName=@ItemName");
                await _connection.ExecuteScalarAsync(sb.ToString(), new { ItemName = _item_name });
            }
        }

        public async Task<int> GetType(string _item_name)
        {
            using (var _connection = this._db.CreateConnection())
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("select ItemType from tbl_items where ItemName=@ItemName");
                return await _connection.QueryFirstOrDefaultAsync<int>(sb.ToString(), new { ItemName = _item_name });
            }
        }

        public async Task Register(ItemDomain domain)
        {
            using (var _connection = this._db.CreateConnection())
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("insert into tbl_items(ItemName,ItemType,ItemContent)");
                sb.AppendLine("values(@ItemName,@ItemType,@ItemContent)");
                await _connection.ExecuteScalarAsync(sb.ToString(), domain);
            }
        }

        public async Task<string> Retrieve(string _item_name)
        {
            using (var _connection = this._db.CreateConnection())
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("select ItemName from tbl_items where ItemName=@ItemName");
                return await _connection.QueryFirstOrDefaultAsync<string>(sb.ToString(), new { ItemName = _item_name });
            }
        }
    }
}
