﻿using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using ObjectOrientedTest.Infrastructure.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
//using Microsoft.
namespace ObjectOrientedTest.Infrastructure
{
    public class DatabaseConnectionFactory : IDatabaseConnectionFactory
    {
        private readonly ApplicationConfiguration _applicationConfiguration;

        public DatabaseConnectionFactory(IOptions<ApplicationConfiguration> options)
        {
            _applicationConfiguration = options.Value;
        }
        public IDbConnection CreateConnection()
        {
            return new MySqlConnection(this._applicationConfiguration.ConnectionStrings.MySqlConnection);
        }
    }
}
