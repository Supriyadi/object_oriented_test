﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Infrastructure.IRepository;
using ObjectOrientedTest.Domain.Entity;


namespace ObjectOrientedTest.Application.Application.UseCase.Register
{
    public class RegisterUseCase : IRegisterUseCase
    {
        private readonly IItemRepository _repository;
        public RegisterUseCase(IItemRepository repository)
        {
            this._repository = repository;
        }
        public async Task Execute(string _item_name, string _item_content, int _item_type)
        {
            ItemDomain domain = new ItemDomain(_item_name, _item_type, _item_content);
            await this._repository.Register(domain);
        }
    }
}
