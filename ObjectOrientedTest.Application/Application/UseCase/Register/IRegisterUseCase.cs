﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Application.Application.UseCase.Register
{
   public interface IRegisterUseCase
    {
        Task Execute(string _item_name, string _item_content, int _item_type);
    }
}
