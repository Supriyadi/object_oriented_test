﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Infrastructure;

namespace ObjectOrientedTest.Application.Application.UseCase.Retrieve
{
    public class RetrieveUseCase : IRetrieveUseCase
    {
        private Infrastructure.IRepository.IItemRepository _repository;
        public RetrieveUseCase(Infrastructure.IRepository.IItemRepository repository)
        {
            this._repository = repository;
        }
        public async Task<string> Execute(string _item_name)
        {
            return await this._repository.Retrieve(_item_name);

        } 
    }
}
