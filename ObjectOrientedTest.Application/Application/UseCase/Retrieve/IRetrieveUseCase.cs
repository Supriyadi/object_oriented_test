﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Application.Application.UseCase.Retrieve
{
   public interface IRetrieveUseCase
    {
        Task<string> Execute(string _item_name);
    }
}
