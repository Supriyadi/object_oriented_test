﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Application.Application.UseCase.GetType
{
   public interface IGetTypeUseCase
    {
        Task<int> Execute(string _item_name); 
    }
}
