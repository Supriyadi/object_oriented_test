﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Infrastructure;

namespace ObjectOrientedTest.Application.Application.UseCase.GetType
{
    public class GetTypeUseCase : IGetTypeUseCase
    {
        private readonly Infrastructure.IRepository.IItemRepository _repository;
        public GetTypeUseCase(Infrastructure.IRepository.IItemRepository repository)
        {
            this._repository = repository;
        }
        public async Task<int> Execute(string _item_name)
        {
            return await this._repository.GetType(_item_name);
        }
    }
}
