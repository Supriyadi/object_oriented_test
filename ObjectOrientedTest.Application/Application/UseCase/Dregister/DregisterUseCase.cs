﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ObjectOrientedTest.Infrastructure.IRepository;

namespace ObjectOrientedTest.Application.Application.UseCase.Dregister
{
    public class DregisterUseCase : IDregisterUseCase
    {
        private readonly IItemRepository _repository;
        public DregisterUseCase(IItemRepository repository)
        {
            this._repository = repository;
        }
        public async Task Execute(string _item_name)
        {
            await this._repository.Dregister(_item_name);
        }
    }
}
