﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Application.Application.UseCase.Dregister
{
   public interface IDregisterUseCase
    {
        Task Execute(string _item_name);
    }
}
