﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObjectOrientedTest.Domain.Entity
{
   public class ItemDomain
    {
        public ItemDomain(string _item_name,int _item_type,string _item_content)
        {
            this.ItemName = _item_name;
            this.ItemType = _item_type;
            this.ItemContent = _item_content;
        }
        public string ItemName { get; set; }
        public int ItemType { get; set; }
        public string ItemContent
        {
            get;set;
        }
    }
}
