using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ObjectOrientedTest.Infrastructure;
using ObjectOrientedTest.Application;
using ObjectOrientedTest.Infrastructure.Options;

namespace ObjectOrientedTest.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
              .AddEnvironmentVariables();

            
            configuration = builder.Build();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region option

            var applicationConfiguration = Configuration.GetSection("ApplicationConfiguration");
            services.Configure<ApplicationConfiguration>(applicationConfiguration);
            //services.AddSingleton<>

            #endregion
            #region Infrastructure
            services.AddSingleton<Infrastructure.IDatabaseConnectionFactory, Infrastructure.DatabaseConnectionFactory>();
            services.AddSingleton<Infrastructure.IRepository.IItemRepository, Infrastructure.Repository.ItemRepository>();
            #endregion

            #region Application
            services.AddSingleton<Application.Application.UseCase.Register.IRegisterUseCase, Application.Application.UseCase.Register.RegisterUseCase>();
            services.AddSingleton<Application.Application.UseCase.Retrieve.IRetrieveUseCase, Application.Application.UseCase.Retrieve.RetrieveUseCase>();
            services.AddSingleton<Application.Application.UseCase.GetType.IGetTypeUseCase, Application.Application.UseCase.GetType.GetTypeUseCase>();
            services.AddSingleton<Application.Application.UseCase.Dregister.IDregisterUseCase, Application.Application.UseCase.Dregister.DregisterUseCase>();
            #endregion

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
