﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObjectOrientedTest.Application.Application.UseCase.Dregister;

namespace ObjectOrientedTest.Web.Controllers.UseCase.Dregister
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IDregisterUseCase _dregister;
        public ItemsController(IDregisterUseCase dregister)
        {
            this._dregister = dregister;
        }

        [HttpDelete]
        [Route("dregister")]
        public async Task<IActionResult>Dregister(string ItemName)
        {
            try
            {
                await this._dregister.Execute(ItemName);
                return Ok(new Helper.GenerallReturn<string>(1, "success", null, "Data deleted successfully !"));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Helper.GenerallReturn<string>(0, "error", null, e.Message.ToString()));
            }
        }
    }
}