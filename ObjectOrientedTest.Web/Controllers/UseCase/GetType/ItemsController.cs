﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObjectOrientedTest.Application;

namespace ObjectOrientedTest.Web.Controllers.UseCase.GetType
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly Application.Application.UseCase.GetType.IGetTypeUseCase _get_type;
        public ItemsController(Application.Application.UseCase.GetType.IGetTypeUseCase get_type)
        {
            this._get_type = get_type;
        }

        [HttpGet]
        [Route("get_type")]
        public async Task<IActionResult> GetType(string ItemName)
        {
            try
            {
                int data = await this._get_type.Execute(ItemName);
                return Ok(new Helper.GenerallReturn<int>(1, "success", data, "data load successfully"));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Helper.GenerallReturn<int>(0, "error", 0, e.Message.ToString()));
            }
        }
    }
}