﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Web.Controllers.UseCase.Register
{
    public class RegisterParam
    {
        public string ItemName { get; set; }
        public int ItemType { get; set; }
        public string ItemContent
        {
            get; set;
        }
    }
}
