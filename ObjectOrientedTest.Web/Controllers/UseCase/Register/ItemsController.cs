﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObjectOrientedTest.Application;
 

namespace ObjectOrientedTest.Web.Controllers.UseCase.Register
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly Application.Application.UseCase.Register.IRegisterUseCase _register;
        public ItemsController(Application.Application.UseCase.Register.IRegisterUseCase register)
        {
            this._register = register;
        }

        [HttpPost]
        [Route("register")]
        public async Task< IActionResult>Register(RegisterParam param)
        {
            try
            {
                await this._register.Execute(param.ItemName, param.ItemContent, param.ItemType);
                return Ok(new Helper.GenerallReturn<string>(1, "success", null, "Data submited successfully !"));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Helper.GenerallReturn<string>(0, "error", null, e.Message.ToString()));
            }
        }
    }
}