﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObjectOrientedTest.Application;

namespace ObjectOrientedTest.Web.Controllers.UseCase.Retrieve
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly Application.Application.UseCase.Retrieve.IRetrieveUseCase _retreive;
        public ItemsController(Application.Application.UseCase.Retrieve.IRetrieveUseCase retrieve)
        {
            this._retreive = retrieve;
        }

        [HttpGet]
        [Route("retreive")]
        public async Task<IActionResult>Retreive(string ItemName)
        {
            try
            {
                string data = await this._retreive.Execute(ItemName);
                return Ok(new Helper.GenerallReturn<string>(1, "success", data, "data load successfully"));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Helper.GenerallReturn<string>(0, "error", null, e.Message.ToString()));
            }
        }

    }
}