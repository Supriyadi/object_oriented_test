﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Web.Helper
{
    public class GenerallReturn<T>
    {
        public GenerallReturn(int _count,string _status,T _data,string _message)
        {
            this.Count = _count;
            this.Status = _status;
            this.Data = _data;
            this.Message = _message;
        }
        public int Count { get; set; }
        public string Status { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }

    }
}
