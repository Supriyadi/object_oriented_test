﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObjectOrientedTest.Web.Helper.Services
{
   public interface IGenerrallReturnServices<T>
    {
        Task<T> GetAllGenerallReturn();
    }
}
